﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SetRiseSun.Views
{
    /// <summary>
    /// Logika interakcji dla klasy DaneHistoryczne.xaml
    /// </summary>
    public partial class DaneHistoryczne : Page
    {
        public DaneHistoryczne()
        {
            InitializeComponent();
        }

        private async void BtnShow_Click(object sender, RoutedEventArgs e)
        {
            double lat = double.Parse(edttextLatitude.Text);
            double lng = double.Parse(edttextLongitude.Text);
            int rrrr = int.Parse(edttextyear.Text);
            int mm = int.Parse(edttextmoth.Text);
            int dd = int.Parse(edttextday.Text);

            if ((lat > 90) || (lng > 180))
            {
                MessageBox.Show("Twoje dane nie odpowiadają współrzędnym ziemi. Zostaną zasymulowane");
            }



            string url = "https://api.sunrise-sunset.org/json?lat=" + lat + "&lng=" + lng + "&date=" + rrrr + "-" + mm + "-" + dd + "" ;

            var sunInfo = await SunProcessor.LoadSunInformation(url);

            edttextSunRise.Text = $"{sunInfo.Sunrise.ToLocalTime().ToShortTimeString()}";
            edttextSunSet.Text = $"{sunInfo.Sunset.ToLocalTime().ToShortTimeString()}";
            edttextDayLength.Text = $"{sunInfo.day_length.ToShortTimeString()}";
            edttextSolarNoon.Text = $"{sunInfo.solar_noon.ToLocalTime().ToShortTimeString()}";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationWindow win = new NavigationWindow();
            win.Content = new MainWindow();
            win.Show();
        }

        private void EdttextLatitude_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextLatitude.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextLatitude.Text = edttextLatitude.Text.Remove(edttextLatitude.Text.Length - 1);
            }
        }

        private void EdttextLongitude_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextLongitude.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextLongitude.Text = edttextLongitude.Text.Remove(edttextLongitude.Text.Length - 1);
            }
        }

        private void Edttextday_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextday.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextday.Text = edttextday.Text.Remove(edttextday.Text.Length - 1);
            }
        }

        private void Edttextmoth_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextmoth.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextmoth.Text = edttextmoth.Text.Remove(edttextmoth.Text.Length - 1);
            }
        }

        private void Edttextyear_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextyear.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextyear.Text = edttextyear.Text.Remove(edttextyear.Text.Length - 1);
            }
        }
    }
}
