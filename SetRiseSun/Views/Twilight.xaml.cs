﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SetRiseSun.Views
{
    /// <summary>
    /// Logika interakcji dla klasy Twilight.xaml
    /// </summary>
    public partial class Twilight : Page
    {
        public Twilight()
        {
            InitializeComponent();
        }

        private async void BtnShow_Click(object sender, RoutedEventArgs e)
        {
            double lat = double.Parse(edttextLatitude.Text);
            double lng = double.Parse(edttextLongitude.Text);

            if ((lat > 90) || (lng > 180))
            {
                MessageBox.Show("Twoje dane nie odpowiadają współrzędnym ziemskim. Zostaną zasymulowane");
            }

            string url = "https://api.sunrise-sunset.org/json?lat=" + lat + "&lng=" + lng + "&date=today";


            var sunInfo = await SunProcessor.LoadSunInformation(url);

            edttextstartcivil.Text = $"{sunInfo.civil_twilight_begin.ToLocalTime().ToShortTimeString()}";
            edttextstartnautical.Text = $"{sunInfo.nautical_twilight_begin.ToLocalTime().ToShortTimeString()}";
            edttextstartastronoical.Text = $"{sunInfo.astronomical_twilight_begin.ToShortTimeString()}";
            edttextendcivil.Text = $"{sunInfo.civil_twilight_end.ToLocalTime().ToShortTimeString()}";
            edttextendnautical.Text = $"{sunInfo.nautical_twilight_end.ToLocalTime().ToShortTimeString()}";
            edttextendastronoical.Text = $"{sunInfo.astronomical_twilight_end.ToShortTimeString()}";
        }

        private void EdttextLatitude_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextLatitude.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextLatitude.Text = edttextLatitude.Text.Remove(edttextLatitude.Text.Length - 1);
            }
        }

        private void EdttextLongitude_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(edttextLongitude.Text, "[^0-9]"))
            {
                MessageBox.Show("Wpisuj tylko liczby");
                edttextLongitude.Text = edttextLongitude.Text.Remove(edttextLongitude.Text.Length - 1);
            }
        }
    }
}
